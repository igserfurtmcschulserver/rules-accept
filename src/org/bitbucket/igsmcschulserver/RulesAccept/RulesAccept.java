package org.bitbucket.igsmcschulserver.RulesAccept;

import org.bitbucket.igelborstel.igelcore.config.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class RulesAccept extends JavaPlugin
{
	Config config;
	
	@Override
	public void onEnable()
	{
		
		this.loadConfig();
		System.out.println("[RulesAccept] Config succesfully loaded!");
		System.out.println("[RulesAccept] Password is set to " + this.getConfig().getString("rulesaccept.password"));
		if(this.getConfig().getString("lang").equals("en_En"))
		{
			config = new Config(this, "en_En", "lang");
			this.setEnglishDefault();
		}
		else if(this.getConfig().getString("lang").equals("de_De"))
		{
			config = new Config(this, "de_De", "lang");
			this.setGermanDefault();
		}
	}
	
	

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = null;
		if(sender instanceof Player)
		{
			p = (Player) sender;
		}
		
		// /rulesaccept reloadcfg
		// l�dt die Config neu
		if(cmd.getName().equalsIgnoreCase("rulesaccept"))
		{
			if(args.length != 1)
			{
				return false;
			}
			if(p == null)
			{
				if(args[0].equalsIgnoreCase("reloadcfg"))
				{
					sender.sendMessage("[RulesAccept] " + this.config.getConfig().getString("lang.reload.start"));
					this.loadConfig();
					sender.sendMessage("[RulesAccept] " + this.config.getConfig().getString("lang.reload.reloaded"));
					return true;
				}
				else if(args[0].equalsIgnoreCase("settp"))
				{
					sender.sendMessage("[RulesAccept] " + this.config.getConfig().getString("lang.console.warnings..settp"));
					return true;
				}
				else if(args[0].equalsIgnoreCase("tp"))
				{
					sender.sendMessage("[RulesAccept] " + this.config.getConfig().getString("lang.console.warnings..tp"));
					return true;
				}
				return false;
			}
			else
			{
				if(args[0].equalsIgnoreCase("reloadcfg"))
				{
					if(p.hasPermission("rulesaccept.reloadcfg") || p.isOp())
					{
						sender.sendMessage(this.config.getConfig().getString("lang.reload.start"));
						this.loadConfig();
						sender.sendMessage(this.config.getConfig().getString("lang.reload.reloaded"));
						return true;
					}
					return false;
				}
				else if(args[0].equalsIgnoreCase("settp"))
				{
					if(p.hasPermission("rulesaccept.settp") || p.isOp())
					{
						double x, y, z, yaw, pitch;
						x = p.getLocation().getX();
						y = p.getLocation().getY();
						z = p.getLocation().getZ();
						yaw = p.getLocation().getYaw();
						pitch = p.getLocation().getPitch();
						String world = p.getLocation().getWorld().getName();
						this.setTpCfg(world, x, y, z, yaw, pitch);
						sender.sendMessage("[Rulesaccpet] " + this.config.getConfig().getString("lang.manage.settp"));
						return true;
					}
					return false;
				}
				else if(args[0].equalsIgnoreCase("tp"))
				{
					if(p.hasPermission("rulesaccept.tp") || p.isOp())
					{
						sender.sendMessage("[Rulesaccept] " + this.config.getConfig().getString("lang.manage.tp"));
						Location warpLoc = new Location(Bukkit.getServer().getWorld(this.getConfig().getString("rulesaccept.playertp.world")), this.getConfig().getDouble("rulesaccept.playertp.x"), this.getConfig().getDouble("rulesaccept.playertp.y"), this.getConfig().getDouble("rulesaccept.playertp.z"), ((float)this.getConfig().getDouble("rulesaccept.playertp.yaw")), ((float)this.getConfig().getDouble("rulesaccept.playertp.pitch")));
						p.teleport(warpLoc);
						return true;
					}
					return false;
				}
				return false;
			}
		}
		// /accept <password>
		
		if(cmd.getName().equalsIgnoreCase("accept"))
		{
			if(args.length != 1)
			{
				return false;
			}
			if(p != null)
			{
				PermissionUser permUser = PermissionsEx.getUser(p);
				boolean defaultUser = true;
				for(String group : permUser.getGroupNames())
				{
					if(!group.equals(this.getConfig().getString("rulesaccept.newbiegroup")))
					{
						defaultUser = false;
					}
				}
				if(!defaultUser)
				{
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.config.getConfig().getString("lang.accept.secondaccept")));
					return true;
				}
				else
				{
					if(!args[0].equals(this.getConfig().getString("rulesaccept.password")))
					{
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', args[0] + this.config.getConfig().getString("lang.accept.wrongPassword")));
						return true;
					}
					else
					{
						this.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', p.getDisplayName() + this.config.getConfig().getString("lang.accept.acceptBroadcast")));
						String[] userGroups = {this.getConfig().getString("rulesaccept.promoteGroup")};
						permUser.setGroups(userGroups);
						Location warpLoc = new Location(Bukkit.getServer().getWorld(this.getConfig().getString("rulesaccept.playertp.world")), this.getConfig().getDouble("rulesaccept.playertp.x"), this.getConfig().getDouble("rulesaccept.playertp.y"), this.getConfig().getDouble("rulesaccept.playertp.z"), ((float)this.getConfig().getDouble("rulesaccept.playertp.yaw")), ((float)this.getConfig().getDouble("rulesaccept.playertp.pitch")));
						p.teleport(warpLoc);
						return true;
					}
				}
			}
			else
			{
				sender.sendMessage(this.config.getConfig().getString("lang.console.warnings.accept"));
				return false;
			}
		}
		return false;
	}
	
	private void loadConfig()
	{
		this.reloadConfig();
		this.getConfig().options().header("Configfile for RulesAccept");
		this.getConfig().addDefault("rulesaccept.password", "password");
		this.getConfig().addDefault("rulesaccept.newbiegroup", "default");
		this.getConfig().addDefault("rulesaccept.promoteGroup", "Spieler");
		this.getConfig().addDefault("rulesaccept.playertp.x", 100);
		this.getConfig().addDefault("rulesaccept.playertp.y", 100);
		this.getConfig().addDefault("rulesaccept.playertp.z", 100);
		this.getConfig().addDefault("rulesaccept.playertp.world", "world");
		this.getConfig().addDefault("rulesaccept.playertp.yaw", 1);
		this.getConfig().addDefault("rulesaccept.playertp.pitch", 1);
		this.getConfig().addDefault("lang", "en_En");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}
	
	private void setTpCfg(String world, double x, double y, double z, double yaw, double pitch)
	{
		this.getConfig().set("rulesaccept.playertp.x", x);
		this.getConfig().set("rulesaccept.playertp.y", y);
		this.getConfig().set("rulesaccept.playertp.z", z);
		this.getConfig().set("rulesaccept.playertp.world", world);
		this.getConfig().set("rulesaccept.playertp.yaw", yaw);
		this.getConfig().set("rulesaccept.playertp.pitch", pitch);
		this.saveConfig();
		this.reloadConfig();
	}
	
	private void setEnglishDefault()
	{
		this.config.reloadConfig();
		this.config.getConfig().addDefault("lang.reload.start", "Reload Config");
		this.config.getConfig().addDefault("lang.reload.reloaded", "Reload complete");
		this.config.getConfig().addDefault("lang.console.warnings.accept", "Only players can Accept the rules");
		this.config.getConfig().addDefault("lang.console.warnings.tp", "Only players can teleport");
		this.config.getConfig().addDefault("lang.console.warnings.settp", "Only players can set the Teleportpoint");
		this.config.getConfig().addDefault("lang.manage.settp", "Telportpoint set!");
		this.config.getConfig().addDefault("lang.manage.tp", "Teleport to the acceptpoint");
		this.config.getConfig().addDefault("lang.accept.secondaccept", "You can accept the rules only once");
		this.config.getConfig().addDefault("lang.accept.wrongPassword", "is the wrong password");
		this.config.getConfig().addDefault("lang.accept.acceptBroadcast", " is now a player!");
		this.config.getConfig().options().copyDefaults(true);
		this.config.saveConfig();
	}
	private void setGermanDefault() 
	{
		this.config.reloadConfig();
		this.config.getConfig().addDefault("lang.reload.start", "Lade Konfigdatei neu...");
		this.config.getConfig().addDefault("lang.reload.reloaded", "Neuladen der Konfig abgeschlossen");
		this.config.getConfig().addDefault("lang.console.warnings.accept", "Nur Spieler koennen die Regeln akzeptieren!");
		this.config.getConfig().addDefault("lang.console.warnings.tp", "Nur Spieler koennen sich teleportieren");
		this.config.getConfig().addDefault("lang.console.warnings.settp", "Nur Spieler koennen den Teleportpunkt setzen");
		this.config.getConfig().addDefault("lang.manage.settp", "Teleportpunkt gesetzt!");
		this.config.getConfig().addDefault("lang.manage.tp", "Teleportiere zum Freischaltpunkt");
		this.config.getConfig().addDefault("lang.accept.secondaccept", "Du kannst dich nur einmal freischalten!");
		this.config.getConfig().addDefault("lang.accept.wrongPassword", " ist das falsche Passwort");
		this.config.getConfig().addDefault("lang.accept.acceptBroadcast", " &2wurde Erfolgreich zum Spieler!");
		this.config.getConfig().options().copyDefaults(true);
		this.config.saveConfig();
	}
}
